from rest_framework import serializers

from .models import Tournament


class SimpleTournamentSerializer(serializers.ModelSerializer):
    player_count = serializers.IntegerField()

    class Meta:
        model = Tournament
        exclude = ('players',)


class FullTournamentSerializer(serializers.ModelSerializer):
    player_count = serializers.IntegerField()

    class Meta:
        model = Tournament
        fields = "__all__"


class CreateTournamentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tournament
        exclude = ('players', 'organizer_vekn_id')
        event_id = None


class TournamentRegistrationRequestSerializer(serializers.Serializer):
    decklist = serializers.CharField(max_length=5120, allow_blank=True)


class TournamentRegistrationResponseSerializer(serializers.Serializer):
    tournament_id = serializers.IntegerField()
