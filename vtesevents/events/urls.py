from django.urls import path

from .views import (
    TournamentRegistrationView,
    TournamentDetailView,
    TournamentView,
    TournamentUsersView,
    TournamentCloseRegistrationView,
    TournamentOpenRegistrationView,
    VeknTournamentInfo,
)

urlpatterns = [
    path('tournaments', TournamentView.as_view(), name="tournament"),
    path('tournaments/<int:tournament_id>', TournamentDetailView.as_view(), name="tournament_details"),
    path('tournaments/<int:tournament_id>/registration', TournamentRegistrationView.as_view(),
         name="tournament_registration"),
    path('tournaments/<int:pk>/users/<int:user_id>', TournamentUsersView.as_view(), name="tournament_users"),
    path('tournaments/<int:pk>/open', TournamentOpenRegistrationView.as_view(), name="tournament_open_registration"),
    path('tournaments/<int:pk>/close', TournamentCloseRegistrationView.as_view(), name="tournament_close_registration"),
    path('tournaments/vekn/<int:tournament_vekn_id>', VeknTournamentInfo.as_view(), name="vekn_tournament_info"),
]
