from datetime import timedelta
from unittest import mock

from django.test import TestCase, Client
from django.urls import reverse
from django.utils import timezone
from rest_framework.test import APIClient

from events.models import Tournament, TournamentUser, TournamentUserStatus
from users.models import User
from vtesevents.test_helpers import build_authenticated_client


def mocked_vekn_events(event_id):
    return [
        {
            "event_id": f"{event_id}",
            "event_name": "Domingo de Shamblings",
            "event_startdate": "2023-04-16",
            "event_starttime": "10:00:00",
            "event_enddate": "2023-04-16",
            "event_endtime": "20:00:00",
            "event_isonline": "0",
            "organizer_veknid": "3190100",
            "venue_id": "2723",
            "venue_name": "Desperta Ferro",
            "venue_city": "Palma de Mallorca",
            "venue_country": "ES",
            "eventtype_id": "2",
            "eventtype_name": "Standard Constructed",
            "rounds": "2R+F",
            "attendance": 10,
            "players": []
        }
    ]


def mocked_vekn_venue(event_id):
    return [
        {
            "name": "",
            "address": "Somewhere",
            "notes": "",
            "zip": "",
            "email": "",
            "phone": "",
            "website": "",
            "city": "",
            "country": "",
            "lat": "",
            "lng": ""
        }
    ]


class TournamentCreateTestCase(TestCase):

    @classmethod
    def setUpClass(cls):
        super(TournamentCreateTestCase, cls).setUpClass()
        cls.user = User.objects.create_user("TestUser", "test@test.com", "Hola1234")
        cls.user.validated_at = timezone.now()
        cls.user.vekn_id = "3190100"
        cls.user.save()

    def setUp(self):
        self.client = build_authenticated_client(self.user.username, "Hola1234")

    @mock.patch('utils.vekn.Client.get_events', side_effect=mocked_vekn_events)
    def test_create_tournament_already_created(self, _):
        rq = {
            "eventId": "10766",
            "name": "Wellcome to the Darkness",
            "date": "2023-05-28",
            "time": "10:00",
            "venueName": "Desperta Ferro",
            "address": "Somewhere",
            "city": "Palma de Mallorca",
            "country": "ES",
            "eventType": "Standard Constructed",
            "rounds": "2R+F",
            "proxiesAllowed": True,
            "registrationOpen": True,
            "registrationFee": "10€",
            "showDeckPolicy": "F",
        }

        rs = self.client.post(reverse("tournament"), rq, format="json")
        self.assertEqual(200, rs.status_code)

        rs = self.client.post(reverse("tournament"), rq, format="json")
        self.assertEqual(400, rs.status_code)
        self.assertEqual('tournament_already_exists', rs.json()['error'])

    def test_anonymous_user_cannot_create_tournaments(self):
        rq = {
            "eventId": "10766",
            "name": "Wellcome to the Darkness",
            "date": "2023-05-28",
            "time": "10:00",
            "venueName": "Desperta Ferro",
            "address": "Somewhere",
            "city": "Palma de Mallorca",
            "country": "ES",
            "eventType": "Standard Constructed",
            "rounds": "2R+F",
            "proxiesAllowed": True,
            "registrationOpen": True,
            "registrationFee": "10€",
            "showDeckPolicy": "F",
        }

        rs = APIClient().post(reverse("tournament"), rq, format="json")

        self.assertEqual(401, rs.status_code)

    @mock.patch('utils.vekn.Client.get_events', side_effect=mocked_vekn_events)
    def test_user_must_be_vekn_organizer(self, _):
        rq = {
            "eventId": "10766",
            "name": "Wellcome to the Darkness",
            "date": "2023-05-28",
            "time": "10:00",
            "venueName": "Desperta Ferro",
            "address": "Somewhere",
            "city": "Palma de Mallorca",
            "country": "ES",
            "eventType": "Standard Constructed",
            "rounds": "2R+F",
            "proxiesAllowed": True,
            "registrationOpen": True,
            "registrationFee": "10€",
            "showDeckPolicy": "F",
        }

        self.user.vekn_id = "0"
        self.user.save()

        rs = self.client.post(reverse("tournament"), rq, format="json")

        self.assertEqual(401, rs.status_code)
        self.assertEqual("tournament_not_owned", rs.json()["error"])

    @mock.patch('utils.vekn.Client.get_events', side_effect=mocked_vekn_events)
    def test_tournament_is_created_properly(self, _):
        expected_response = {
            'id': 1,
            'eventId': '10766',
            'organizerVeknId': '3190100',
            'name': 'Wellcome to the Darkness',
            'date': "2023-05-28",
            'time': "10:00",
            'venueName': 'Desperta Ferro',
            'address': 'Somewhere',
            'city': 'Palma de Mallorca',
            'country': 'ES',
            'eventType': 'Standard Constructed',
            'rounds': '2R+F',
            'proxiesAllowed': True,
            'playerCount': 0,
            'registrationOpen': True,
            'registrationFee': '10€',
            'showDeckPolicy': 'F',
            'description': None,
            'email': None,
            'website': None,
            'phoneNumber': None
        }

        rq = {
            "eventId": "10766",
            "name": "Wellcome to the Darkness",
            "date": "2023-05-28",
            "time": "10:00",
            "venueName": "Desperta Ferro",
            "address": "Somewhere",
            "city": "Palma de Mallorca",
            "country": "ES",
            "eventType": "Standard Constructed",
            "rounds": "2R+F",
            "proxiesAllowed": True,
            "registrationOpen": True,
            "registrationFee": "10€",
            "showDeckPolicy": "F",
        }

        rs = self.client.post(reverse("tournament"), rq, format="json")

        self.assertEqual(200, rs.status_code)
        self.assertDictEqual(expected_response, rs.json())
        self.assertTrue(Tournament.objects.filter(event_id="10766").exists())


class TournamentRegistrationTestCase(TestCase):

    @classmethod
    def setUpClass(cls):
        super(TournamentRegistrationTestCase, cls).setUpClass()
        cls.user = User.objects.create_user("TestUser", "test@test.com", "Hola1234")
        cls.user.validated_at = timezone.now()
        cls.user.save()

    def setUp(self):
        self.tournament = Tournament.objects.create(
            event_id="123",
            organizer_vekn_id="4123",
            name="Test Tournament",
            date=timezone.now() + timedelta(days=1),
            time="10:00",
            venue_name="Casa Xavi",
            address="Si",
            city="Montreal",
            country="Powerbase",
            event_type="?",
            rounds="3R+F",
            proxies_allowed=True,
            show_deck_policy='F'
        )
        self.client = build_authenticated_client(self.user.username, "Hola1234")
        self.empty_rq = {'decklist': ''}

    def test_anonymous_user_gets_an_error_when_registers_to_tournament(self):
        self.client.credentials()
        rs = self.client.post(reverse("tournament_registration", args=[self.tournament.pk]), self.empty_rq)

        self.assertEqual(401, rs.status_code)
        self.assertEqual(0, self.tournament.players.count())

    def test_user_registers_to_tournament(self):
        decklist = """My Beloved Decklist
        Crypt
        ==================
        90x Tupdog
        
        Library
        ==================
        90x Villein"""

        rq = {'decklist': decklist}
        rs = self.client.post(reverse("tournament_registration", args=[self.tournament.pk]), rq)

        self.assertEqual(200, rs.status_code)
        self.assertEqual(self.tournament.pk, rs.data["tournament_id"])

        self.assertIn(self.user, self.tournament.players.all())

        tournament_user = TournamentUser.objects.filter(user=self.user, tournament=self.tournament).get()
        self.assertEqual(TournamentUserStatus.REGISTERED, tournament_user.status)

        stored_decklist = tournament_user.decklist
        self.assertEqual(decklist, stored_decklist)

    def test_user_registers_to_closed_tournament(self):
        self.tournament.registration_open = False
        self.tournament.save()

        rs = self.client.post(reverse("tournament_registration", args=[self.tournament.pk]), self.empty_rq)

        self.assertEqual(423, rs.status_code)
        self.assertEqual("tournament_closed", rs.data["error"])

        self.assertNotIn(self.user, self.tournament.players.all())

    def test_anonymous_user_unregister_from_tournament(self):
        self.client.credentials()
        rs = self.client.delete(reverse("tournament_registration", args=[self.tournament.pk]))

        self.assertEqual(401, rs.status_code)
        self.assertEqual(0, self.tournament.players.count())

    def test_user_unregisters_from_tournament(self):
        TournamentUser.objects.create(
            tournament=self.tournament,
            user=self.user,
            status=TournamentUserStatus.REGISTERED
        )

        rs = self.client.delete(reverse("tournament_registration", args=[self.tournament.pk]))
        self.assertEqual(200, rs.status_code)
        self.assertEqual(self.tournament.pk, rs.data["tournament_id"])

        self.assertNotIn(self.user, self.tournament.players.all())

    def test_user_unregisters_from_closed_tournament(self):
        TournamentUser.objects.create(
            tournament=self.tournament,
            user=self.user,
            status=TournamentUserStatus.REGISTERED
        )

        self.tournament.registration_open = False
        self.tournament.save()

        rs = self.client.delete(reverse("tournament_registration", args=[self.tournament.pk]))
        self.assertEqual(423, rs.status_code)
        self.assertEqual("tournament_closed", rs.data["error"])

        self.assertIn(self.user, self.tournament.players.all())

    def test_user_unregister_from_tournament_when_it_is_not_registered(self):
        rs = self.client.delete(reverse("tournament_registration", args=[self.tournament.pk]))
        self.assertEqual(200, rs.status_code)
        self.assertEqual(self.tournament.pk, rs.data["tournament_id"])

        self.assertNotIn(self.user, self.tournament.players.all())

    def test_user_updates_decklist(self):
        decklist = """My Beloved Decklist
                Crypt
                ==================
                90x Tupdog

                Library
                ==================
                90x Villein"""

        new_decklist = """My Beloved Decklist V2
                Crypt
                ==================
                150x Tupdog

                Library
                ==================
                60x Villein"""

        rs = self.client.post(reverse("tournament_registration", args=[self.tournament.pk]), {'decklist': decklist})
        self.assertEqual(200, rs.status_code)

        rs = self.client.post(reverse("tournament_registration", args=[self.tournament.pk]), {'decklist': new_decklist})
        self.assertEqual(200, rs.status_code)

        stored_decklist = TournamentUser.objects.filter(user=self.user, tournament=self.tournament).get().decklist
        self.assertEqual(new_decklist, stored_decklist)


class TournamentRetrieveTestCase(TestCase):

    @classmethod
    def setUpClass(cls):
        super(TournamentRetrieveTestCase, cls).setUpClass()
        cls.user = User.objects.create_user("TestUser", "test@test.com", "Hola1234")
        cls.user.validated_at = timezone.now()
        cls.user.save()

    def setUp(self):
        self.tournament = Tournament.objects.create(
            event_id="123",
            organizer_vekn_id="4123",
            name="Test Tournament",
            date=timezone.now().date() + timedelta(days=1),
            time="10:00",
            venue_name="Casa Xavi",
            address="Si",
            city="Montreal",
            country="Powerbase",
            event_type="?",
            rounds="3R+F",
            proxies_allowed=True,
            show_deck_policy='F'
        )

        self.client = build_authenticated_client(self.user.username, "Hola1234")

    def test_tournament_returns_player_count(self):
        rs = self.client.get(reverse("tournament_details", args=[self.tournament.pk]))
        self.assertEqual(200, rs.status_code)
        self.assertEqual(0, rs.json()["playerCount"])

        TournamentUser.objects.create(
            tournament=self.tournament,
            user=self.user,
            status=TournamentUserStatus.REGISTERED
        )

        rs = self.client.get(reverse("tournament_details", args=[self.tournament.pk]))
        self.assertEqual(200, rs.status_code)
        self.assertEqual(1, rs.json()["playerCount"])

        TournamentUser.objects.update(
            tournament=self.tournament,
            user=self.user,
            status=TournamentUserStatus.PLAYING
        )

        rs = self.client.get(reverse("tournament_details", args=[self.tournament.pk]))
        self.assertEqual(200, rs.status_code)
        self.assertEqual(1, rs.json()["playerCount"])

        TournamentUser.objects.update(
            tournament=self.tournament,
            user=self.user,
            status=TournamentUserStatus.NO_SHOW
        )

        rs = self.client.get(reverse("tournament_details", args=[self.tournament.pk]))
        self.assertEqual(200, rs.status_code)
        self.assertEqual(0, rs.json()["playerCount"])

    def test_tournament_list_returns_player_count(self):
        rs = self.client.get(reverse("tournament"))
        self.assertEqual(200, rs.status_code)
        self.assertEqual(0, rs.json()["results"][0]["playerCount"])

        TournamentUser.objects.create(
            tournament=self.tournament,
            user=self.user,
            status=TournamentUserStatus.REGISTERED
        )

        rs = self.client.get(reverse("tournament"))
        self.assertEqual(200, rs.status_code)
        self.assertEqual(1, rs.json()["results"][0]["playerCount"])

        TournamentUser.objects.update(
            tournament=self.tournament,
            user=self.user,
            status=TournamentUserStatus.PLAYING
        )

        rs = self.client.get(reverse("tournament"))
        self.assertEqual(200, rs.status_code)
        self.assertEqual(1, rs.json()["results"][0]["playerCount"])

        TournamentUser.objects.update(
            tournament=self.tournament,
            user=self.user,
            status=TournamentUserStatus.NO_SHOW
        )

        rs = self.client.get(reverse("tournament"))
        self.assertEqual(200, rs.status_code)
        self.assertEqual(0, rs.json()["results"][0]["playerCount"])

    def test_tournament_list_filters_by_datetime_range(self):
        from_param = self.tournament.date + timedelta(days=1)
        to_param = from_param + timedelta(days=1)

        rs = self.client.get(reverse("tournament"), {"from": from_param, "to": to_param})
        self.assertEqual(200, rs.status_code)
        self.assertEqual(0, rs.json()["count"])

    def test_tournament_list_from_param_default_min(self):
        to_param = self.tournament.date

        rs = self.client.get(reverse("tournament"), {"to": to_param})
        self.assertEqual(200, rs.status_code)
        self.assertEqual(1, rs.json()["count"])

    def test_tournament_list_to_param_default_max(self):
        from_param = self.tournament.date

        rs = self.client.get(reverse("tournament"), {"from": from_param})
        self.assertEqual(200, rs.status_code)
        self.assertEqual(1, rs.json()["count"])

    def test_tournament_list_returns_registered_value_if_user_authenticated(self):
        Tournament.objects.create(
            event_id="222",
            organizer_vekn_id="4123",
            name="Test Tournament 2",
            date=timezone.now().date() + timedelta(days=2),
            time="10:00",
            venue_name="Casa Xavi",
            address="Si",
            city="Montreal",
            country="Powerbase",
            event_type="?",
            rounds="3R+F",
            proxies_allowed=True
        )

        TournamentUser.objects.create(
            tournament=self.tournament,
            user=self.user,
            status=TournamentUserStatus.REGISTERED
        )

        rs = self.client.get(reverse("tournament"))
        self.assertEqual(200, rs.status_code)
        rs_body = rs.json()
        for tournament in rs_body["results"]:
            self.assertIn("isRegistered", tournament)
            is_registered = Tournament.objects.get(pk=tournament["id"]).players.contains(self.user)
            self.assertEqual(is_registered, tournament["isRegistered"])

    def test_tournament_list_returns_registered_null_if_user_is_anonymous(self):
        self.client.credentials()
        self.tournament = Tournament.objects.create(
            event_id="222",
            organizer_vekn_id="4123",
            name="Test Tournament 2",
            date=timezone.now() + timedelta(days=1),
            time="10:00",
            venue_name="Casa Xavi",
            address="Si",
            city="Montreal",
            country="Powerbase",
            event_type="?",
            rounds="3R+F",
            proxies_allowed=True
        )

        TournamentUser.objects.create(
            tournament=self.tournament,
            user=self.user,
            status=TournamentUserStatus.REGISTERED
        )

        rs = self.client.get(reverse("tournament"))

        self.assertEqual(200, rs.status_code)
        rs_body = rs.json()
        for tournament in rs_body["results"]:
            self.assertIn("isRegistered", tournament)
            self.assertIsNone(tournament["isRegistered"])

    def test_tournament_details_returns_registered_field_true_if_user_is_registered(self):
        TournamentUser.objects.create(
            tournament=self.tournament,
            user=self.user,
            status=TournamentUserStatus.REGISTERED
        )

        rs = self.client.get(reverse("tournament_details", args=[self.tournament.pk]))

        self.assertEqual(200, rs.status_code)
        rs_body = rs.json()
        self.assertIn("isRegistered", rs_body)
        self.assertTrue(rs_body["isRegistered"])

    def test_tournament_details_returns_registered_field_false_if_user_is_registered(self):
        rs = self.client.get(reverse("tournament_details", args=[self.tournament.pk]))

        self.assertEqual(200, rs.status_code)
        rs_body = rs.json()
        self.assertIn("isRegistered", rs_body)
        self.assertFalse(rs_body["isRegistered"])

    def test_tournament_details_returns_registered_field_none_if_user_is_anonymous(self):
        self.client.credentials()
        rs = self.client.get(reverse("tournament_details", args=[self.tournament.pk]))

        self.assertEqual(200, rs.status_code)
        rs_body = rs.json()
        self.assertIn("isRegistered", rs_body)
        self.assertIsNone(rs_body["isRegistered"])

        TournamentUser.objects.create(
            tournament=self.tournament,
            user=self.user,
            status=TournamentUserStatus.REGISTERED
        )
        rs = self.client.get(reverse("tournament_details", args=[self.tournament.pk]))

        self.assertEqual(200, rs.status_code)
        rs_body = rs.json()
        self.assertIn("isRegistered", rs_body)
        self.assertIsNone(rs_body["isRegistered"])


class TournamentUsersTestCase(TestCase):

    @classmethod
    def setUpClass(cls):
        super(TournamentUsersTestCase, cls).setUpClass()
        cls.user = User.objects.create_user("TestUser", "test@test.com", "Hola1234")
        cls.user.validated_at = timezone.now()
        cls.user.vekn_id = "111111"
        cls.user.save()

    def setUp(self):
        self.tournament = Tournament.objects.create(
            event_id="123",
            organizer_vekn_id=self.user.vekn_id,
            name="Test Tournament",
            date=timezone.now() + timedelta(days=1),
            time="10:00",
            venue_name="Casa Xavi",
            address="Si",
            city="Montreal",
            country="Powerbase",
            event_type="?",
            rounds="3R+F",
            proxies_allowed=True
        )

        self.user_2 = User.objects.create_user("TestUser 2", "test2@test.com", "Hola1234")
        self.user_2.validated_at = timezone.now()
        self.user_2.vekn_id = "222222"
        self.user_2.save()

        self.client = build_authenticated_client(self.user.username, "Hola1234")

    def test_organizers_adds_user(self):
        rs = self.client.post(reverse("tournament_users", args=[self.tournament.pk, self.user_2.pk]))

        self.assertEqual(200, rs.status_code)
        self.assertIn(self.user_2.pk, rs.json()["players"])
        self.assertTrue(TournamentUser.objects
                        .filter(tournament=self.tournament, user=self.user_2, status=TournamentUserStatus.REGISTERED)
                        .exists()
                        )

    def test_organizers_adds_user_twice(self):
        rs = self.client.post(reverse("tournament_users", args=[self.tournament.pk, self.user_2.pk]))
        self.assertEqual(200, rs.status_code)

        rs = self.client.post(reverse("tournament_users", args=[self.tournament.pk, self.user_2.pk]))
        self.assertEqual(200, rs.status_code)

        self.assertIn(self.user_2.pk, rs.json()["players"])
        self.assertEqual(1, TournamentUser.objects
                         .filter(tournament=self.tournament, user=self.user_2, status=TournamentUserStatus.REGISTERED)
                         .count()
                         )

    def test_organizers_removes_user(self):
        TournamentUser.objects.create(
            tournament=self.tournament,
            user=self.user_2,
            status=TournamentUserStatus.REGISTERED
        )

        rs = self.client.delete(reverse("tournament_users", args=[self.tournament.pk, self.user_2.pk]))

        self.assertEqual(200, rs.status_code)
        self.assertNotIn(self.user_2.pk, rs.json()["players"])
        self.assertFalse(TournamentUser.objects
                         .filter(tournament=self.tournament, user=self.user_2)
                         .exists()
                         )

    def test_organizers_removes_user_twice(self):
        TournamentUser.objects.create(
            tournament=self.tournament,
            user=self.user_2,
            status=TournamentUserStatus.REGISTERED
        )

        rs = self.client.delete(reverse("tournament_users", args=[self.tournament.pk, self.user_2.pk]))
        self.assertEqual(200, rs.status_code)

        rs = self.client.delete(reverse("tournament_users", args=[self.tournament.pk, self.user_2.pk]))

        self.assertEqual(200, rs.status_code)
        self.assertNotIn(self.user_2.pk, rs.json()["players"])
        self.assertFalse(TournamentUser.objects
                         .filter(tournament=self.tournament, user=self.user_2)
                         .exists()
                         )

    def test_non_organizer_adds_and_remove_user(self):
        client = build_authenticated_client(self.user_2.username, "Hola1234")

        rs = client.post(reverse("tournament_users", args=[self.tournament.pk, self.user_2.pk]))
        self.assertEqual(403, rs.status_code)

        rs = client.delete(reverse("tournament_users", args=[self.tournament.pk, self.user_2.pk]))
        self.assertEqual(403, rs.status_code)


class TournamentRegistrationManagementTestCase(TestCase):

    @classmethod
    def setUpClass(cls):
        super(TournamentRegistrationManagementTestCase, cls).setUpClass()
        cls.user = User.objects.create_user("TestUser", "test@test.com", "Hola1234")
        cls.user.validated_at = timezone.now()
        cls.user.vekn_id = "4123"
        cls.user.save()

        cls.non_organizer_user = User.objects.create_user("TestUser2", "test2@test.com", "Hola1234")
        cls.non_organizer_user.validated_at = timezone.now()
        cls.non_organizer_user.vekn_id = "222222"
        cls.non_organizer_user.save()

    def setUp(self):
        self.tournament = Tournament.objects.create(
            event_id="123",
            organizer_vekn_id="4123",
            name="Test Tournament",
            date=timezone.now() + timedelta(days=1),
            time="10:00",
            venue_name="Casa Xavi",
            address="Si",
            city="Montreal",
            country="Powerbase",
            event_type="?",
            rounds="3R+F",
            proxies_allowed=True
        )
        self.client = build_authenticated_client(self.user.username, "Hola1234")
        self.non_organizer_client = build_authenticated_client(self.non_organizer_user.username, "Hola1234")

    def test_organizer_opens_tournament_registration(self):
        self.tournament.registration_open = False
        self.tournament.save()

        rs = self.client.post(reverse("tournament_open_registration", args=[self.tournament.pk]))
        self.tournament.refresh_from_db()

        self.assertEqual(200, rs.status_code)
        self.assertTrue(self.tournament.registration_open)

    def test_organizer_closes_tournament_registration(self):
        self.assertTrue(self.tournament.registration_open)

        rs = self.client.post(reverse("tournament_close_registration", args=[self.tournament.pk]))
        self.tournament.refresh_from_db()

        self.assertEqual(200, rs.status_code)
        self.assertFalse(self.tournament.registration_open)

    def test_non_organizer_opens_tournament_registration(self):
        self.tournament.registration_open = False
        self.tournament.save()

        rs = self.non_organizer_client.post(reverse("tournament_open_registration", args=[self.tournament.pk]))
        self.tournament.refresh_from_db()

        self.assertEqual(403, rs.status_code)
        self.assertFalse(self.tournament.registration_open)

    def test_non_organizer_closes_tournament_registration(self):
        self.assertTrue(self.tournament.registration_open)

        rs = self.non_organizer_client.post(reverse("tournament_close_registration", args=[self.tournament.pk]))
        self.tournament.refresh_from_db()

        self.assertEqual(403, rs.status_code)
        self.assertTrue(self.tournament.registration_open)


class VeknTournamentInfoTestCase(TestCase):

    @classmethod
    def setUpClass(cls):
        super(VeknTournamentInfoTestCase, cls).setUpClass()
        cls.user = User.objects.create_user("TestUser", "test@test.com", "Hola1234")
        cls.user.validated_at = timezone.now()
        cls.user.vekn_id = "4123"
        cls.user.save()

    def setUp(self):
        self.client = build_authenticated_client(self.user.username, "Hola1234")

    def test_anonymous_user_cannot_get_vekn_tournament_info(self):
        rs = APIClient().get(reverse("vekn_tournament_info", args=["000000"]))
        self.assertEqual(401, rs.status_code)

    @mock.patch('utils.vekn.Client.get_events', side_effect=mocked_vekn_events)
    def test_non_organizer_user_cannot_get_vekn_tournament_info(self, _):
        self.user.vekn_id = "123"
        self.user.save()

        rs = self.client.get(reverse("vekn_tournament_info", args=["123123"]), format="json")

        self.assertEqual(401, rs.status_code)
        self.assertEqual("tournament_not_owned", rs.json()["error"])

    @mock.patch('utils.vekn.Client.get_events', side_effect=mocked_vekn_events)
    @mock.patch('utils.vekn.Client.get_venue', side_effect=mocked_vekn_venue)
    def test_organizer_gets_tournament_info_from_vekn(self, _, __):
        self.user.vekn_id = "3190100";
        self.user.save()
        expected_response = {
            'eventId': '123123',
            'organizerVeknId': '3190100',
            'name': 'Domingo de Shamblings',
            'date': "2023-04-16",
            'time': "10:00:00",
            'venueName': 'Desperta Ferro',
            'address': 'Somewhere',
            'city': 'Palma de Mallorca',
            'country': 'ES',
            'eventType': 'Standard Constructed',
            'rounds': '2R+F',
            'proxiesAllowed': False,
            'registrationFee': '',
        }

        rs = self.client.get(reverse("vekn_tournament_info", args=["123123"]), format="json")

        self.assertEqual(200, rs.status_code)
        self.assertDictEqual(expected_response, rs.json())
