from rest_framework.permissions import BasePermission


class IsTournamentOrganizer(BasePermission):

    def has_object_permission(self, request, view, obj):
        return request.user.vekn_id == obj.organizer_vekn_id
