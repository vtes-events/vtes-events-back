
class VeknEventMapper:

    @staticmethod
    def to_tournament(event_data, venue_data):
        return {
            'event_id': event_data['event_id'],
            'organizer_vekn_id': event_data['organizer_veknid'],
            'name': event_data['event_name'],
            'date': event_data['event_startdate'],
            'time': event_data['event_starttime'],
            'venue_name': event_data['venue_name'],
            'address': venue_data['address'] if venue_data else "",
            'city': event_data['venue_city'],
            'country': event_data['venue_country'],
            'registration_fee': "",
            'proxies_allowed': False,  # TODO: When VEKN API updated use event['proxies_allowed']
            'rounds': event_data['rounds'],
            'event_type': event_data['eventtype_name'],
        }
