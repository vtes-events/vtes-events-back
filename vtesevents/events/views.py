from django.shortcuts import get_object_or_404
from django.utils import timezone
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema, no_body
from flags.state import flag_disabled
from rest_framework import generics, pagination
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from events.models import Tournament, TournamentUser, TournamentUserStatus
from events.permissions import IsTournamentOrganizer
from events.serializers import (
    SimpleTournamentSerializer,
    FullTournamentSerializer,
    TournamentRegistrationRequestSerializer,
    TournamentRegistrationResponseSerializer,
    CreateTournamentSerializer
)
from events.mappers import VeknEventMapper
from utils.errors import VTESEventsException
from utils.vekn import api_client as vekn_client
import logging

from_param = openapi.Parameter('from', openapi.IN_QUERY, description="Date for filtering (YYYY-MM-DD)",
                               type=openapi.TYPE_STRING, required=False)
to_param = openapi.Parameter('to', openapi.IN_QUERY, description="Date for filtering (YYYY-MM-DD)",
                             type=openapi.TYPE_STRING, required=False)


def _is_organizer(user_vekn_id, vekn_event_id):
    events = vekn_client.get_events(vekn_event_id)
    if not events:
        raise VTESEventsException(status.HTTP_404_NOT_FOUND, "vekn_tournament_not_found")

    if flag_disabled("IGNORE_IMPORT_TOURNAMENT_VEKN_CHECK"):
        return events[0]["organizer_veknid"] == user_vekn_id
    else:
        return True


class TournamentDetailView(generics.GenericAPIView):
    queryset = Tournament.objects.all()
    serializer_class = FullTournamentSerializer

    def get(self, request, tournament_id):
        tournament = get_object_or_404(self.queryset, pk=tournament_id)
        serializer = self.get_serializer(tournament)
        rs_data = serializer.data

        if request.user.is_authenticated:
            is_registered = tournament.players.contains(request.user)
        else:
            is_registered = None
        rs_data["is_registered"] = is_registered

        return Response(rs_data, status=status.HTTP_200_OK)


class TournamentView(generics.GenericAPIView):
    queryset = Tournament.objects.all()
    serializer_class = SimpleTournamentSerializer
    pagination_class = pagination.LimitOffsetPagination

    @swagger_auto_schema(manual_parameters=[from_param, to_param])
    def get(self, request):
        from_value = request.GET.get("from", timezone.datetime.min.date())
        to_value = request.GET.get("to", timezone.datetime.max.date())

        tournaments = self.queryset.filter(date__range=(from_value, to_value))
        page = self.paginate_queryset(tournaments)
        serializer = self.get_serializer(page, many=True)
        rs_data = serializer.data

        for tournament_data in rs_data:
            if request.user.is_authenticated:
                is_registered = tournaments.get(pk=tournament_data['id']).players.contains(request.user)
            else:
                is_registered = None
            tournament_data["is_registered"] = is_registered

        return self.get_paginated_response(serializer.data)

    def post(self, request):
        if not request.user.is_authenticated:
            return Response(status=401)

        tournament_serializer = CreateTournamentSerializer(data=request.data)
        if tournament_serializer.is_valid():
            event_vekn_id = request.data["event_id"]
            user_vekn_id = request.user.vekn_id
            if _is_organizer(user_vekn_id, event_vekn_id):
                new_tournament = tournament_serializer.create(tournament_serializer.validated_data)
                new_tournament.organizer_vekn_id = user_vekn_id
                new_tournament.save()

                return Response(SimpleTournamentSerializer(new_tournament).data, status=status.HTTP_200_OK)
            else:
                raise VTESEventsException(401, 'tournament_not_owned')
        elif 'event_id' in tournament_serializer.errors:
            raise VTESEventsException(400, 'tournament_already_exists')
        else:
            return Response(tournament_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class TournamentRegistrationView(generics.GenericAPIView):
    queryset = Tournament.objects.all()
    serializer_class = TournamentRegistrationResponseSerializer
    permission_classes = [IsAuthenticated]

    @swagger_auto_schema(
        request_body=TournamentRegistrationRequestSerializer,
        responses={
            '200': TournamentRegistrationResponseSerializer,
            '423': 'Tournament locked'
        }
    )
    def post(self, request, tournament_id):
        rq_serializer = TournamentRegistrationRequestSerializer(data=request.data)
        if not rq_serializer.is_valid():
            return Response(rq_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        tournament = get_object_or_404(self.queryset, pk=tournament_id)
        if tournament.is_open():
            TournamentUser.objects.update_or_create(
                tournament=tournament,
                user=request.user,
                status=TournamentUserStatus.REGISTERED,
                defaults={"decklist": rq_serializer.data['decklist']}
            )
            return Response(TournamentRegistrationResponseSerializer({"tournament_id": int(tournament_id)}).data,
                            status=status.HTTP_200_OK)
        else:
            raise VTESEventsException(status.HTTP_423_LOCKED, "tournament_closed")

    @swagger_auto_schema(
        request_body=no_body,
        responses={
            '200': TournamentRegistrationResponseSerializer,
            '423': 'Tournament locked'
        }
    )
    def delete(self, request, tournament_id):
        tournament = get_object_or_404(self.queryset, pk=tournament_id)
        if tournament.is_open():
            TournamentUser.objects.filter(
                tournament=tournament,
                user=request.user
            ).delete()
            return Response(TournamentRegistrationResponseSerializer({"tournament_id": int(tournament_id)}).data,
                            status=status.HTTP_200_OK)
        else:
            raise VTESEventsException(status.HTTP_423_LOCKED, "tournament_closed")


class TournamentUsersView(generics.GenericAPIView):
    queryset = Tournament.objects.all()
    serializer_class = FullTournamentSerializer
    permission_classes = [IsAuthenticated, IsTournamentOrganizer]

    @swagger_auto_schema(request_body=no_body)
    def post(self, request, pk, user_id):
        tournament = self.get_object()
        TournamentUser.objects.update_or_create(
            tournament=tournament,
            user_id=user_id,
            status=TournamentUserStatus.REGISTERED
        )
        return Response(self.get_serializer(tournament).data, status=200)

    @swagger_auto_schema(request_body=no_body)
    def delete(self, request, pk, user_id):
        tournament = self.get_object()
        TournamentUser.objects.filter(
            tournament=tournament,
            user_id=user_id,
        ).delete()

        return Response(self.get_serializer(tournament).data, status=200)


class TournamentCloseRegistrationView(generics.GenericAPIView):
    queryset = Tournament.objects.all()
    permission_classes = [IsAuthenticated, IsTournamentOrganizer]
    serializer_class = SimpleTournamentSerializer

    @swagger_auto_schema(request_body=no_body, responses={200: ""})
    def post(self, request, pk):
        tournament = self.get_object()
        tournament.registration_open = False
        tournament.save()

        return Response(status=200)


class TournamentOpenRegistrationView(generics.GenericAPIView):
    queryset = Tournament.objects.all()
    permission_classes = [IsAuthenticated, IsTournamentOrganizer]
    serializer_class = SimpleTournamentSerializer

    @swagger_auto_schema(request_body=no_body, responses={200: ""})
    def post(self, request, pk):
        tournament = self.get_object()
        tournament.registration_open = True
        tournament.save()

        return Response(status=200)


class VeknTournamentInfo(generics.GenericAPIView):
    serializer_class = SimpleTournamentSerializer
    permission_classes = [IsAuthenticated]

    def get(self, request, tournament_vekn_id):
        events = vekn_client.get_events(tournament_vekn_id)
        if not events:
            raise VTESEventsException(status.HTTP_404_NOT_FOUND, "vekn_tournament_not_found")

        if flag_disabled("IGNORE_IMPORT_TOURNAMENT_VEKN_CHECK"):
            if events[0]["organizer_veknid"] != request.user.vekn_id:
                raise VTESEventsException(401, "tournament_not_owned")

        venue_data = self.__get_venue_data(events[0]['venue_id'])

        tournament_data = VeknEventMapper.to_tournament(events[0], venue_data)
        serializer = self.serializer_class(data=tournament_data)

        return Response(serializer.initial_data, status=status.HTTP_200_OK)

    def __get_venue_data(self, venue_id):
        try:
            venues = vekn_client.get_venue(venue_id)
        except Exception as ex:
            logging.warning(f"Error getting venue {venue_id}", ex)
            return None

        if not venues:
            return None

        return venues[0]


