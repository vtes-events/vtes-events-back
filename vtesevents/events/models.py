from django.db import models

from users.models import User


class TournamentUserStatus(models.TextChoices):
    REGISTERED = 'R'
    PLAYING = 'P'
    DROP = 'D'
    NO_SHOW = 'NS'


class ShowDecklistPolicy(models.TextChoices):
    ALL = 'A'
    FINALISTS = 'F'
    WINNER = 'W'
    NONE = 'N'


class Tournament(models.Model):
    event_id = models.CharField(max_length=16, unique=True, blank=False)
    organizer_vekn_id = models.CharField(max_length=16, blank=False, null=False)
    name = models.CharField(max_length=256, blank=False, null=False)
    email = models.EmailField(blank=True, null=True)
    description = models.TextField(max_length=5120, blank=True, null=True)
    date = models.DateField(default=None, blank=False, null=False)
    time = models.CharField(max_length=5, blank=False, null=False)
    venue_name = models.CharField(max_length=128, blank=True, null=True)
    address = models.CharField(max_length=128, blank=False, null=False)
    city = models.CharField(max_length=64, blank=False, null=False)
    country = models.CharField(max_length=64, blank=False, null=False)
    registration_fee = models.CharField(max_length=16, blank=False, null=False)
    proxies_allowed = models.BooleanField(null=False)
    rounds = models.CharField(max_length=16, blank=False, null=False)
    phone_number = models.CharField(max_length=64, blank=True, null=True)
    website = models.CharField(max_length=64, blank=True, null=True)
    event_type = models.CharField(max_length=64, blank=False, null=False)
    registration_open = models.BooleanField(default=True, blank=False, null=False)
    show_deck_policy = models.CharField(max_length=2, choices=ShowDecklistPolicy.choices)

    players = models.ManyToManyField(User, through='TournamentUser', blank=True)

    def is_open(self):
        return self.registration_open

    def player_count(self):
        return self.players.filter(
            tournamentuser__status__in=[TournamentUserStatus.REGISTERED, TournamentUserStatus.PLAYING]
        ).count()

    def __str__(self):
        return self.name


class TournamentUser(models.Model):
    tournament = models.ForeignKey(Tournament, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    status = models.CharField(max_length=2, choices=TournamentUserStatus.choices)
    decklist = models.TextField(default="", max_length=5120, blank=True)

    class Meta:
        unique_together = ('tournament', 'user',)
