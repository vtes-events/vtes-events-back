from django.contrib import admin

from .models import Tournament, TournamentUser


class TournamentPlayer(admin.TabularInline):
    model = TournamentUser


class TournamentAdmin(admin.ModelAdmin):
    inlines = [
        TournamentPlayer,
    ]


admin.site.register(Tournament, TournamentAdmin)
