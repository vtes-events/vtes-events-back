import logging

import requests

from vtesevents.settings import TELEGRAM_BOT_TOKEN, TELEGRAM_CHANNEL_ID

URL = f"https://api.telegram.org/bot{TELEGRAM_BOT_TOKEN}/sendMessage"

MESSAGE_TEMPLATE = """
*{0} ( {1} )*

{2} 
"""


class NotificationError(Exception):
    pass


def notify_by_telegram(name, email, message):
    message = MESSAGE_TEMPLATE.format(name, email, message)
    rs = requests.get(URL, params={'chat_id': TELEGRAM_CHANNEL_ID, 'text': message, "parse_mode": "markdown"})
    if rs.status_code != 200:
        logging.error(f"[Contact] Error notifying via Telegram: {rs.json()}")
        raise NotificationError()
