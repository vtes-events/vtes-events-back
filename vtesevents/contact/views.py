from drf_yasg.utils import swagger_auto_schema
from rest_framework import generics, status
from rest_framework.response import Response

from contact import notification
from contact.notification import NotificationError
from contact.serializers import ContactRequestSerializer
from utils import captcha
from utils.errors import VTESEventsException


class ContactView(generics.GenericAPIView):
    serializer_class = ContactRequestSerializer

    @swagger_auto_schema(responses={200: ""})
    def post(self, request):
        if captcha.is_valid(request.data['captcha_token']):
            serializer = self.serializer_class(data=request.data)
            if serializer.is_valid():
                try:
                    notification.notify_by_telegram(
                        serializer.validated_data["name"],
                        serializer.validated_data["email"],
                        serializer.validated_data["message"]
                    )
                except NotificationError:
                    raise VTESEventsException(status.HTTP_500_INTERNAL_SERVER_ERROR, "error_sending_message")

                return Response(status=status.HTTP_200_OK)
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        else:
            raise VTESEventsException(status.HTTP_400_BAD_REQUEST, "invalid_captcha")
