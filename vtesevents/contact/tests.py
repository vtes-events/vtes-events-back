from unittest import mock

from django.test import TestCase
from django.urls import reverse

data = {
    "captcha_token": "",
    "name": "Pepito",
    "email": "pepito@gmail.com",
    "message": "Lorem fistrum hasta luego Lucas pupita ese que llega pecador ese pedazo de condemor ahorarr "
               "pecador va usté muy cargadoo ese hombree. Está la cosa muy malar se calle ustée mamaar por la "
               "gloria de mi madre va usté muy cargadoo de la pradera te va a hasé pupitaa se calle ustée."
}


class ContactTest(TestCase):

    @mock.patch('contact.notification.notify_by_telegram')
    @mock.patch('utils.captcha.is_valid', side_effect=lambda x: True)
    def test_contact(self, captcha, telegram_notify):
        rs = self.client.post(reverse("contact"), data)

        self.assertEqual(200, rs.status_code)
        captcha.assert_called()
        telegram_notify.assert_called_with(data["name"], data["email"], data["message"])

    @mock.patch('contact.notification.notify_by_telegram')
    @mock.patch('utils.captcha.is_valid', side_effect=lambda x: False)
    def test_contact_without_captcha(self, captcha, telegram_notify):
        rs = self.client.post(reverse("contact"), data)

        self.assertEqual(400, rs.status_code)
        captcha.assert_called()
        telegram_notify.assert_not_called()
