from rest_framework import serializers


class ContactRequestSerializer(serializers.Serializer):
    name = serializers.CharField(min_length=1, max_length=64)
    email = serializers.EmailField()
    message = serializers.CharField(min_length=1, max_length=1024)
