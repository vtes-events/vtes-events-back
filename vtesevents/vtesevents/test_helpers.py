from django.test import Client
from rest_framework.test import APIClient
from django.urls import reverse
from unittest import mock


def build_authenticated_client(username, password):
    rq = {"username": username, "password": password, 'captchaToken': 'dummy'}
    with mock.patch('utils.captcha.is_valid', side_effetct=lambda x: True):
        rs = Client().post(reverse("login"), rq, content_type="application/json")
    token = rs.data['access']
    client = APIClient()
    client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    return  client
