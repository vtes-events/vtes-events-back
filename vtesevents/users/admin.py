from django.contrib import admin

from custom_auth.views import ResetPasswordView
from users.models import User
from users.views import UserView


def send_validation_email(modeladmin, request, queryset):
    for user in queryset.iterator():
        UserView.send_validation_mail(user)


def reset_password(modeladmin, request, queryset):
    for user in queryset.iterator():
        ResetPasswordView.send_reset_password(user)


class UserAdmin(admin.ModelAdmin):
    actions = [send_validation_email, reset_password]


admin.site.register(User, UserAdmin)
