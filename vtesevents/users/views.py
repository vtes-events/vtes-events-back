from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.shortcuts import get_object_or_404
from django.utils import timezone
from drf_yasg.utils import swagger_auto_schema
from rest_framework import generics, pagination
from rest_framework import status
from rest_framework.exceptions import NotAuthenticated
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from users.models import User
from users.serializers import UserSerializer, CreateUserSerializer, UserVerificationSerializer, SimpleUserSerializer
from utils import captcha, mail
from utils.errors import VTESEventsException
from utils.vekn import api_client


class UserView(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = SimpleUserSerializer
    pagination_class = pagination.LimitOffsetPagination

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return super().get(request, *args, **kwargs)
        else:
            raise NotAuthenticated()

    @swagger_auto_schema(request_body=CreateUserSerializer, responses={200: UserSerializer})
    def post(self, request):
        if captcha.is_valid(request.data['captcha_token']):
            players = api_client.get_users(request.data['vekn_id'])
            user_serializer = CreateUserSerializer(players, data=request.data)
            if user_serializer.is_valid():
                new_user = user_serializer.create(user_serializer.validated_data)
                self.send_validation_mail(new_user)
                return Response(UserSerializer(new_user).data, status=status.HTTP_200_OK)
            else:
                if 'vekn_id' in user_serializer.errors:
                    raise VTESEventsException(status.HTTP_400_BAD_REQUEST, 'vekn_id_already_registered')
                elif 'email' in user_serializer.errors:
                    raise VTESEventsException(status.HTTP_400_BAD_REQUEST, 'email_already_registered')
                elif 'username' in user_serializer.errors:
                    raise VTESEventsException(status.HTTP_400_BAD_REQUEST, 'username_already_registered')
                else:
                    return Response(status.HTTP_400_BAD_REQUEST, user_serializer.errors)

        else:
            raise VTESEventsException(status.HTTP_400_BAD_REQUEST, "invalid_captcha")

    @staticmethod
    def send_validation_mail(user):
        token_generator = PasswordResetTokenGenerator()
        token = token_generator.make_token(user)
        mail.send_validation_email(user, token)


class RetrieveUserView(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = SimpleUserSerializer
    permission_classes = [IsAuthenticated]


class UserSelfView(generics.GenericAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [IsAuthenticated]

    def get(self, request):
        user = User.objects.get(pk=request.user.pk)
        serializer = UserSerializer(user)
        return Response(serializer.data, status=status.HTTP_200_OK)


class UserVerificationView(generics.GenericAPIView):
    queryset = User.objects.all()
    serializer_class = UserVerificationSerializer

    def post(self, request, user_id):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            user = get_object_or_404(self.queryset, pk=user_id)
            token_validator = PasswordResetTokenGenerator()
            if token_validator.check_token(user, serializer.validated_data["token"]):
                if not user.is_verified():
                    user.validated_at = timezone.now()
                    user.save()
                return Response(status=status.HTTP_200_OK)

            else:
                raise VTESEventsException(status.HTTP_400_BAD_REQUEST, "invalid_token")
        else:
            raise VTESEventsException(status.HTTP_400_BAD_REQUEST, "")
