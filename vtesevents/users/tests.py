from datetime import datetime, timedelta
from unittest import mock

from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.test import TestCase
from django.urls import reverse
from django.utils import timezone
from freezegun import freeze_time

from users.models import User
from vtesevents.test_helpers import build_authenticated_client

mocked_player = {
    "veknid": "1111111",
    "firstname": "Pepito",
    "lastname": "Popote",
    "city": "Palma",
    "statename": "Illes Balears",
    "countryname": "Spain"
}

mocked_player_2 = {
    "veknid": "0247000",
    "firstname": "Jorgita",
    "lastname": "Popota",
    "city": "Mexico city",
    "statename": "",
    "countryname": "Mexico"
}


def raise_exception(x):
    raise Exception("Boom!")


class UserRegisterTestCase(TestCase):

    @mock.patch('users.models.User.email_user')
    @mock.patch('utils.vekn.Client.get_users', side_effect=lambda x: [mocked_player])
    @mock.patch('utils.captcha.is_valid', side_effect=lambda x: True)
    def test_user_registers(self, captcha_mock, vekn_mock, mail_mock):
        rq_body = {
            "username": "NewUser",
            "email": "new@user.com",
            "veknId": "1111111",
            "password": "Hola1234",
            "captchaToken": "",
        }
        rs = self.client.post(reverse("users"), rq_body, format="json")
        self.assertEqual(200, rs.status_code)

        new_user = User.objects.get(email="new@user.com")
        self.assertFalse(new_user.is_verified())
        self.assertIsNotNone(new_user.last_validation_mail_sent)

        mail_mock.assert_called_once()

    @mock.patch('users.models.User.email_user', side_effect=raise_exception)
    @mock.patch('utils.vekn.Client.get_users', side_effect=lambda x: [mocked_player])
    @mock.patch('utils.captcha.is_valid', side_effect=lambda x: True)
    def test_user_registers_doesnt_fail_if_validation_mail_fails(self, captcha_mock, vekn_mock, mail_mock):
        rq_body = {
            "username": "NewUser",
            "email": "new@user.com",
            "veknId": "1111111",
            "password": "Hola1234",
            "captchaToken": "",
        }
        rs = self.client.post(reverse("users"), rq_body, format="json")
        self.assertEqual(200, rs.status_code)

        new_user = User.objects.get(email="new@user.com")
        self.assertIsNone(new_user.last_validation_mail_sent)

        mail_mock.assert_called_once()

    @mock.patch('utils.mail.send_validation_email')
    @mock.patch('utils.captcha.is_valid', side_effect=lambda x: False)
    def test_user_registration_fails_if_captcha_is_invalid(self, captcha_mock, mail_mock):
        rq_body = {
            "username": "NoneUser",
            "email": "NoneUser@user.com",
            "veknId": "0000000",
            "password": "Hola1234",
            "captchaToken": "",
        }
        rs = self.client.post(reverse("users"), rq_body, format="json")
        self.assertEqual(400, rs.status_code)
        self.assertEqual("invalid_captcha", rs.json()['error'])
        self.assertEqual(0, User.objects.filter(email="NoneUser@user.com").count())
        mail_mock.assert_not_called()

    @mock.patch('utils.mail.send_validation_email')
    @mock.patch('utils.vekn.Client.get_users', side_effect=lambda x: [])
    @mock.patch('utils.captcha.is_valid', side_effect=lambda x: True)
    def test_user_registration_fails_if_vekn_id_doesnt_exist(self, captcha_mock, vekn_mock, mail_mock):
        rq_body = {
            "username": "NoneUser",
            "email": "NoneUser@user.com",
            "veknId": "0000000",
            "password": "Hola1234",
            "captchaToken": "",
        }
        rs = self.client.post(reverse("users"), rq_body, format="json")
        self.assertEqual(400, rs.status_code)
        self.assertEqual("vekn_id_validation_failed", rs.json()['error'])
        self.assertEqual(0, User.objects.filter(email="NoneUser@user.com").count())
        mail_mock.assert_not_called()

    @mock.patch('utils.mail.send_validation_email')
    @mock.patch('utils.vekn.Client.get_users', side_effect=lambda x: [mocked_player])
    @mock.patch('utils.captcha.is_valid', side_effect=lambda x: True)
    def test_user_registration_fails_if_registers_twice(self, captcha_mock, vekn_mock, mail_mock):
        user = User.objects.create_user("User2", "user2@test.com", "Hola1234")
        user.validated_at = timezone.now()
        user.vekn_id = "2222222"
        user.save()

        rq_body = {
            "username": "User2",
            "email": "user2@user.com",
            "veknId": "2222222",
            "password": "Hola1234",
            "captchaToken": "",
        }
        rs = self.client.post(reverse("users"), rq_body, format="json")
        self.assertEqual(400, rs.status_code)
        self.assertEqual("vekn_id_already_registered", rs.json()['error'])
        self.assertEqual(0, User.objects.filter(email="NoneUser@user.com").count())
        mail_mock.assert_not_called()

    @mock.patch('utils.mail.send_validation_email')
    @mock.patch('utils.vekn.Client.get_users', side_effect=lambda x: [mocked_player])
    @mock.patch('utils.captcha.is_valid', side_effect=lambda x: True)
    def test_user_registration_fails_if_username_already_registered(self, captcha_mock, vekn_mock, mail_mock):
        user = User.objects.create_user("User2", "user2@test.com", "Hola1234")
        user.validated_at = timezone.now()
        user.vekn_id = "2222222"
        user.save()

        rq_body = {
            "username": "User2",
            "email": "user2_2@user.com",
            "veknId": "0247000",
            "password": "Hola1234",
            "captchaToken": "",
        }
        rs = self.client.post(reverse("users"), rq_body, format="json")
        self.assertEqual(400, rs.status_code)
        self.assertEqual("username_already_registered", rs.json()['error'])
        self.assertEqual(0, User.objects.filter(email="NoneUser@user.com").count())
        mail_mock.assert_not_called()

    @mock.patch('utils.mail.send_validation_email')
    @mock.patch('utils.vekn.Client.get_users', side_effect=lambda x: [mocked_player_2])
    @mock.patch('utils.captcha.is_valid', side_effect=lambda x: True)
    def test_user_registration_fails_if_email_already_registered(self, captcha_mock, vekn_mock, mail_mock):
        user = User.objects.create_user("User2", "user2@test.com", "Hola1234")
        user.validated_at = timezone.now()
        user.vekn_id = "2222222"
        user.save()

        rq_body = {
            "username": "User2_2",
            "email": "user2@test.com",
            "veknId": "0247000",
            "password": "Hola1234",
            "captchaToken": "",
        }
        rs = self.client.post(reverse("users"), rq_body, format="json")
        self.assertEqual(400, rs.status_code)
        self.assertEqual("email_already_registered", rs.json()['error'])
        self.assertEqual(0, User.objects.filter(email="NoneUser@user.com").count())
        mail_mock.assert_not_called()

    @mock.patch('utils.mail.send_validation_email')
    @mock.patch('utils.vekn.Client.get_users', side_effect=lambda x: [mocked_player])
    @mock.patch('utils.captcha.is_valid', side_effect=lambda x: True)
    def test_user_registration_fails_if_vekn_id_already_registered(self, captcha_mock, vekn_mock, mail_mock):
        user = User.objects.create_user("User2", "user2@test.com", "Hola1234")
        user.validated_at = timezone.now()
        user.vekn_id = "2222222"
        user.save()

        rq_body = {
            "username": "User2_2",
            "email": "user2_2@user.com",
            "veknId": "2222222",
            "password": "Hola1234",
            "captchaToken": "",
        }
        rs = self.client.post(reverse("users"), rq_body, format="json")
        self.assertEqual(400, rs.status_code)
        self.assertEqual("vekn_id_already_registered", rs.json()['error'])
        self.assertEqual(0, User.objects.filter(email="NoneUser@user.com").count())
        mail_mock.assert_not_called()


class UserReadTestCase(TestCase):

    @classmethod
    def setUpClass(cls):
        super(UserReadTestCase, cls).setUpClass()

    def setUp(self):
        self.user = User.objects.create_user("TestUser", "test2@test.com", "Hola1234")
        self.user.first_name = "Foo"
        self.user.last_name = "Bar"
        self.user.vekn_id = "1111113"
        self.user.validated_at = timezone.now()
        self.user.save()

        self.client = build_authenticated_client(self.user.username, "Hola1234")

    def test_get_self_user_details(self):
        expected_rs = {
            'id': self.user.pk,
            'username': self.user.username,
            'email': self.user.email,
            'firstName': self.user.first_name,
            'lastName': self.user.last_name,
            'veknId': self.user.vekn_id
        }

        rs = self.client.get(reverse("users_self"), format="json")
        self.assertEqual(200, rs.status_code)
        self.assertDictEqual(expected_rs, rs.json())

    def test_get_random_user_info_if_authenticated(self):
        expected_rs = {
            'id': self.user.pk,
            'username': self.user.username,
            'firstName': self.user.first_name,
            'lastName': self.user.last_name,
            'veknId': self.user.vekn_id
        }

        rs = self.client.get(reverse("users_detail", args=[self.user.pk]), format="json")
        self.assertEqual(200, rs.status_code)
        self.assertDictEqual(expected_rs, rs.json())

    def test_get_random_user_info_error_if_not_authenticated(self):
        self.client.credentials()
        rs = self.client.get(reverse("users_detail", args=[self.user.pk]), format="json")
        self.assertEqual(401, rs.status_code)

    def test_get_user_list_info_if_authenticated(self):
        expected_user_data = {
            'id': self.user.pk,
            'username': self.user.username,
            'firstName': self.user.first_name,
            'lastName': self.user.last_name,
            'veknId': self.user.vekn_id
        }

        rs = self.client.get(reverse("users"), format="json")

        self.assertEqual(200, rs.status_code)
        rs_data = rs.json()
        self.assertEqual(1, rs_data["count"])
        self.assertDictEqual(expected_user_data, rs_data["results"][0])

    def test_get_user_list_info_error_if_not_authenticated(self):
        self.client.credentials()
        rs = self.client.get(reverse("users"), format="json")
        self.assertEqual(401, rs.status_code)


class UserVerificationTestCase(TestCase):

    def setUp(self):
        self.user = User.objects.create(
            username="TestUser",
            email="test@test.com",
            vekn_id="111111",
            first_name="Foo",
            last_name="Bar"
        )

        self.user.set_password("Hola1234!")
        self.user.save()

    def test_user_verifies_account(self):
        with freeze_time(timezone.now() - timedelta(days=6, hours=22)):
            token_util = PasswordResetTokenGenerator()
            token = token_util.make_token(self.user)

        rq = {"token": token}
        rs = self.client.post(reverse("user_verify", args=[self.user.pk]), rq, content_type="application/json")

        self.assertEqual(200, rs.status_code)

        self.user.refresh_from_db()
        self.assertTrue(self.user.is_verified())

    def test_user_verifies_account_gets_error_if_token_expired(self):
        with freeze_time("2012-01-01"):
            token_util = PasswordResetTokenGenerator()
            token = token_util.make_token(self.user)

        rq = {"userId": self.user.pk, "token": token}
        rs = self.client.post(reverse("user_verify", args=[self.user.pk]), rq, content_type="application/json")

        self.assertEqual(400, rs.status_code)
        self.assertEqual("invalid_token", rs.json()["error"])

        self.user.refresh_from_db()
        self.assertFalse(self.user.is_verified())

    def test_user_verifies_account_gets_error_if_token_is_invalid(self):
        rq = {"userId": self.user.pk, "token": "MyRandomToken123"}
        rs = self.client.post(reverse("user_verify", args=[self.user.pk]), rq, content_type="application/json")

        self.assertEqual(400, rs.status_code)
        self.assertEqual("invalid_token", rs.json()["error"])

        self.user.refresh_from_db()
        self.assertFalse(self.user.is_verified())

    def test_user_verifies_account_gets_ok_if_already_validated(self):
        validated_date = timezone.make_aware(datetime.strptime("01-01-2000", '%d-%m-%Y'))
        self.user.validated_at = validated_date
        self.user.save()

        token_util = PasswordResetTokenGenerator()
        token = token_util.make_token(self.user)

        rq = {"userId": self.user.pk, "token": token}
        rs = self.client.post(reverse("user_verify", args=[self.user.pk]), rq, content_type="application/json")

        self.assertEqual(200, rs.status_code)

        self.user.refresh_from_db()
        self.assertTrue(self.user.is_verified())
        self.assertEqual(validated_date, self.user.validated_at)
