from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils import timezone


class User(AbstractUser):
    vekn_id = models.CharField(max_length=16, unique=True, blank=False)
    validated_at = models.DateTimeField(default=None, blank=True, null=True)
    email = models.EmailField(blank=False, unique=True)
    last_password_reset = models.DateTimeField(blank=True, null=True)
    last_validation_mail_sent = models.DateTimeField(blank=True, null=True)

    def is_verified(self):
        return self.validated_at is not None and self.validated_at <= timezone.now()

    REQUIRED_FIELDS = ["email", "vekn_id"]
