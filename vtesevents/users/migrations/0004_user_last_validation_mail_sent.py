# Generated by Django 4.1.7 on 2023-10-29 11:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0003_user_last_password_reset'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='last_validation_mail_sent',
            field=models.DateTimeField(null=True),
        ),
    ]
