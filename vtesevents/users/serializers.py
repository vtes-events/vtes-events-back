from rest_framework import serializers, status
from rest_framework.validators import UniqueValidator

from users.models import User
from utils.errors import VTESEventsException


class UserSerializer(serializers.ModelSerializer):
    email = serializers.EmailField()

    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'first_name', 'last_name', 'vekn_id')


class SimpleUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'first_name', 'last_name', 'vekn_id')


class CreateUserSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(validators=[UniqueValidator(queryset=User.objects.all())])

    class Meta:
        model = User
        fields = ('username', 'email', 'vekn_id', 'password')

    def __init__(self, external_data=None, data=None):
        super().__init__(data=data)
        self.external_data = external_data

    def validate(self, attrs):
        players = self.external_data
        if len(players) == 0:
            raise VTESEventsException(
                status=status.HTTP_400_BAD_REQUEST,
                error_code='vekn_id_validation_failed',
                message=f"Player not found. [{attrs['vekn_id']}]"
            )

        if len(players) > 1:
            raise VTESEventsException(
                status=status.HTTP_400_BAD_REQUEST,
                error_code='vekn_id_validation_failed',
                message=f"More than one player found. [{attrs['vekn_id']}]"
            )

        player = players[0]
        if player['veknid'] != attrs['vekn_id']:
            raise VTESEventsException(
                status=status.HTTP_400_BAD_REQUEST,
                error_code='vekn_id_validation_failed',
                message=f"ID dot not match. [{attrs['vekn_id']}]"
            )

        attrs['firstName'] = player['firstname']
        attrs['lastName'] = player['lastname']

        return attrs

    def create(self, validated_data):
        user = User.objects.create(
            username=validated_data['username'],
            email=validated_data['email'],
            vekn_id=validated_data['vekn_id'],
            first_name=validated_data['firstName'],
            last_name=validated_data['lastName']
        )

        user.set_password(validated_data['password'])
        user.save()

        return user


class UserVerificationSerializer(serializers.Serializer):
    token = serializers.CharField(min_length=1, max_length=64)
