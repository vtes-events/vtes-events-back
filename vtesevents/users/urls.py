from django.urls import path

from users.views import UserVerificationView, UserSelfView, UserView, RetrieveUserView

urlpatterns = [
    path('users', UserView.as_view(), name="users"),
    path('users/<int:pk>', RetrieveUserView.as_view(), name="users_detail"),
    path('users/self', UserSelfView.as_view(), name="users_self"),

    path('users/<int:user_id>/verify', UserVerificationView.as_view(), name='user_verify'),
]
