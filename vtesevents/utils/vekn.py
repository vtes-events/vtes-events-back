import os
from dataclasses import dataclass

import requests
from requests.adapters import HTTPAdapter, Retry


@dataclass
class VeknError(Exception):
    status_code: int
    message: str


class Client:
    """
    API Docs: https://www.vekn.net/API/readme.txt
    """
    HOST = "https://www.vekn.net"
    LOGIN_ENDPOINT = "/api/vekn/login"
    REGISTRY_ENDPOINT = "/api/vekn/registry"
    EVENT_ENDPOINT = "/api/vekn/event"
    VENUE_ENDPOINT = "/api/vekn/venue"

    def __init__(self, username=None, password=None):
        self.username = username
        self.password = password
        self.auth_token = None

        self.__init_session()

    def __init_session(self):
        self.session = requests.session()
        retry_strategy = Retry(
            total=2,
            backoff_factor=0.1,
            status_forcelist=[413, 429, 500, 502, 503, 504]
        )
        adapter = HTTPAdapter(max_retries=retry_strategy)

        self.session.mount('https://', adapter)

    def login(self):
        body = {'username': self.username, 'password': self.password}
        rs = self.__send_request('POST', self.LOGIN_ENDPOINT, data=body, requires_auth=False)
        self.auth_token = rs['data']['auth']

    def get_users(self, vekn_id_or_name):
        params = {'filter': vekn_id_or_name}
        rs = self.__send_request('GET', self.REGISTRY_ENDPOINT, params=params)
        return rs['data']['players']

    def get_events(self, event_id):
        rs = self.__send_request('GET', f"{self.EVENT_ENDPOINT}/{event_id}")
        return rs['data']['events']

    def get_venue(self, venue_id):
        rs = self.__send_request('GET', f"{self.VENUE_ENDPOINT}/{venue_id}")
        return rs['data']['venues']

    def __send_request(self, method, endpoint, data=None, params=None, requires_auth=True):
        headers = {}
        error_code, error_message = None, None
        retry = True
        retries = 0
        while retry:
            retry = False
            retries += 1

            if requires_auth:
                headers = {"Authorization": f"Bearer {self.auth_token}"}

            rs = self.session.request(method, f"{self.HOST}{endpoint}", data=data, params=params, headers=headers)
            error_code, error_message = self.__extract_error(rs)

            if error_code == 403 and retries < 2 and endpoint != self.LOGIN_ENDPOINT:
                self.login()
                retry = True

        if error_code:
            raise VeknError(int(error_code), error_message)

        return rs.json()

    def __extract_error(self, rs):
        if rs.status_code != 200:
            return rs.status_code, ""

        rs_data = rs.json()
        if rs_data["err_code"]:
            return rs_data["err_code"], rs_data["err_msg"]

        if 'code' in rs_data["data"] and int(rs_data["data"]["code"]) != 200:
            return rs_data["data"]["code"], rs_data["data"]["message"]

        return None, None


api_client = Client(os.getenv("VEKN_USER"), os.getenv("VEKN_PASSWORD"))
