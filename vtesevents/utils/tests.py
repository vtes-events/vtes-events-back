from unittest import mock

from django.test import TestCase

from utils.vekn import Client, VeknError


class MockResponse:
    def __init__(self, json_data, status_code):
        self.json_data = json_data
        self.status_code = status_code

    def json(self):
        return self.json_data


def mock_login_error(method, endpoint, data, params, headers):
    data = {"err_msg": "Username and password do not match or you do not have an account yet.", "err_code": 403,
            "response_id": "", "api": "", "version": "", "data": {}}
    return MockResponse(data, 403)


class VeknClientTestCase(TestCase):

    @mock.patch('requests.Session.request', side_effect=mock_login_error)
    def test_login_do_no_retry_forever(self, _):
        client = Client("user", "password")

        self.assertRaises(VeknError, client.login)
