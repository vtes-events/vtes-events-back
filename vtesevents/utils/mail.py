from django.utils import timezone
import logging


def send_validation_email(user, token):
    try:
        user.email_user(
            subject="Account validation",
            message=f"Your validation link is: https://vtes.events/users/sign_up/verify?user={user.id}&token={token}",
            from_email="no-reply@vtes.events"
        )
        user.last_validation_mail_sent = timezone.now()
        user.save()
    except Exception as ex:
        logging.error("Error sending validation mail", exc_info=ex)


def send_password_reset_email(user, token):
    user.email_user(
        subject="Password reset",
        message=f"Your token for resetting password is: {token}",
        from_email="no-reply@vtes.events"
    )
