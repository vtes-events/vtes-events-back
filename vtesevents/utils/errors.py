from rest_framework.exceptions import APIException
from rest_framework.response import Response

from utils.vekn import VeknError


class VTESEventsException(APIException):

    def __init__(self, status, error_code, message=""):
        super().__init__(detail=message, code=error_code)
        self.status_code = status


def __build_response(status_code, error_code, message):
    response_data = {
        "error": error_code,
        "message": message
    }
    return Response(status=status_code, data=response_data)


def custom_exception_handler(exc, context):
    if isinstance(exc, VeknError):
        return __build_response(exc.status_code, "vekn_error", exc.message)

    if isinstance(exc, APIException):
        return __build_response(exc.status_code, exc.get_codes(), exc.detail)

    return __build_response(500, "unexpected_error", str(exc))
