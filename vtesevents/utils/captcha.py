import os

import requests
from requests.adapters import HTTPAdapter, Retry

HOST = "https://www.google.com/recaptcha/api/siteverify"


def is_valid(token):
    session = requests.session()
    retry_strategy = Retry(
        total=2,
        backoff_factor=0.1,
        status_forcelist=[413, 429, 500, 502, 503, 504]
    )
    adapter = HTTPAdapter(max_retries=retry_strategy)
    session.mount('https://', adapter)

    data = {
        'secret': os.getenv('RECAPTCHA_SECRET_KEY'),
        'response': token,
    }
    rs = session.post(HOST, data=data, )

    rs_data = rs.json()

    return rs_data['success']
