from unittest import mock

from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.test import TestCase, Client
from django.urls import reverse
from django.utils import timezone

from users.models import User
from vtesevents.test_helpers import build_authenticated_client
from freezegun import freeze_time


class LoginTestCase(TestCase):

    @classmethod
    def setUpClass(cls):
        super(LoginTestCase, cls).setUpClass()

    def setUp(self):
        self.user = User.objects.create_user("TestUser", "test2@test.com", "Hola1234")
        self.user.first_name = "Foo"
        self.user.last_name = "Bar"
        self.user.vekn_id = "1111113"
        self.user.save()

    @mock.patch('utils.captcha.is_valid', side_effect=lambda x: True)
    def test_login_error_if_user_not_validated(self, _):
        rq = {"username": "TestUser", "password": "Hola1234", "captchaToken": "si"}
        rs = self.client.post(reverse("login"), rq, content_type="application/json")
        self.assertEqual(401, rs.status_code)
        self.assertEqual("account_not_validated", rs.json()["error"])

    @mock.patch('utils.captcha.is_valid', side_effect=lambda x: False)
    def test_login_error_if_captcha_is_invalid(self, _):
        rq = {"username": "TestUser", "password": "Hola1234", "captchaToken": "si"}
        rs = self.client.post(reverse("login"), rq, content_type="application/json")
        self.assertEqual(400, rs.status_code)
        self.assertEqual("invalid_captcha", rs.json()["error"])


class PasswordChangeTestCase(TestCase):

    def setUp(self):
        self.user = User.objects.create_user("TestUser", "test@test.com", "Hola1234")
        self.user.first_name = "Foo"
        self.user.last_name = "Bar"
        self.user.vekn_id = "1111111"
        self.user.validated_at = timezone.now()
        self.user.save()

        self.client = build_authenticated_client(self.user.username, "Hola1234")

    def test_user_changes_password(self):
        new_password = "ThisIsMyNewPassword"
        rq_body = {
            "oldPassword": "Hola1234",
            "newPassword": new_password,
        }

        rs = self.client.post(reverse("auth_password_change"), rq_body, format="json")
        self.assertEqual(200, rs.status_code)

        self.user.refresh_from_db()
        self.assertTrue(self.user.check_password(new_password))

    def test_anonymous_user_cannot_change_password(self):
        self.client.credentials()
        new_password = "ThisIsMyNewPassword"
        rq_body = {
            "oldPassword": "Hola1234",
            "newPassword": new_password,
        }

        rs = self.client.post(reverse("auth_password_change"), rq_body, format="json")
        self.assertEqual(401, rs.status_code)
        self.assertFalse(self.user.check_password(new_password))

    def test_user_changes_password_using_wrong_old_password(self):
        new_password = "ThisIsMyNewPassword"
        rq_body = {
            "oldPassword": "WrongOldPassword",
            "newPassword": new_password,
        }

        rs = self.client.post(reverse("auth_password_change"), rq_body, format="json")
        self.assertEqual(400, rs.status_code)
        self.assertEqual("invalid_password", rs.json()['error'])

        self.user.refresh_from_db()
        self.assertFalse(self.user.check_password(new_password))


class PasswordResetTestCase(TestCase):

    def setUp(self):
        self.user = User.objects.create_user("TestUser", "test2@test.com", "Hola1234")
        self.user.first_name = "Foo"
        self.user.last_name = "Bar"
        self.user.vekn_id = "1111113"
        self.user.save()

    @mock.patch('django.contrib.auth.tokens.PasswordResetTokenGenerator.make_token', side_effect=lambda x: "abc123")
    @mock.patch('utils.mail.send_password_reset_email')
    def test_password_reset_sends_mail(self, mail, token_generator):
        rq_body = {"email": self.user.email}
        rs = self.client.post(reverse("auth_password_reset"), rq_body, format="json")

        self.assertEqual(200, rs.status_code)
        self.assertIsNone(rs.data)
        mail.assert_called_with(self.user, "abc123")

    @mock.patch('utils.mail.send_password_reset_email')
    def test_password_reset_does_nothing_if_user_not_found(self, mail):
        rq_body = {"email": "doesnotexist@test.com"}
        rs = self.client.post(reverse("auth_password_reset"), rq_body, format="json")

        self.assertEqual(200, rs.status_code)
        self.assertIsNone(rs.data)
        mail.assert_not_called()

    def test_password_reset_confirm_changes_password(self):
        new_password = "PasswordForTokenResetConfirm123!"
        token_generator = PasswordResetTokenGenerator()
        token = token_generator.make_token(self.user)
        rq_body = {
            "email": self.user.email,
            "token": token,
            "newPassword": new_password
        }

        self.assertFalse(self.user.check_password(new_password))

        rs = self.client.post(reverse("auth_password_reset_confirm"), rq_body, format="json")
        self.user.refresh_from_db()

        self.assertEqual(200, rs.status_code)
        self.assertIsNone(rs.data)
        self.assertTrue(self.user.check_password(new_password))

    def test_password_reset_confirm_does_nothing_if_token_invalid(self):
        new_password = "PasswordForTokenResetConfirm123!"
        rq_body = {
            "email": self.user.email,
            "token": "Donimos-Pipsa",
            "newPassword": new_password
        }

        self.assertFalse(self.user.check_password(new_password))

        rs = self.client.post(reverse("auth_password_reset_confirm"), rq_body, format="json")
        self.user.refresh_from_db()

        self.assertEqual(400, rs.status_code)
        self.assertIsNone(rs.data)
        self.assertFalse(self.user.check_password(new_password))

    def test_password_reset_confirm_does_nothing_if_token_belongs_to_another_user(self):
        aux_user = User.objects.create_user("PasswordResetConfirmUser", "PasswordResetConfirmUser@test.com", "Hola1234")
        aux_user.first_name = "Foo"
        aux_user.last_name = "Bar"
        aux_user.vekn_id = "1111115"
        aux_user.save()

        new_password = "PasswordForTokenResetConfirm123!"
        token_generator = PasswordResetTokenGenerator()
        token = token_generator.make_token(aux_user)
        rq_body = {
            "email": self.user.email,
            "token": token,
            "newPassword": new_password
        }

        self.assertFalse(self.user.check_password(new_password))
        self.assertFalse(aux_user.check_password(new_password))

        rs = self.client.post(reverse("auth_password_reset_confirm"), rq_body, format="json")
        self.user.refresh_from_db()
        aux_user.refresh_from_db()

        self.assertEqual(400, rs.status_code)
        self.assertIsNone(rs.data)
        self.assertFalse(self.user.check_password(new_password))
        self.assertFalse(aux_user.check_password(new_password))

    @mock.patch('utils.mail.send_password_reset_email')
    def test_password_reset_does_nothing_if_already_reseted_last_hour(self, mail):
        rq_body = {"email": self.user.email}

        with freeze_time("2020-01-01 00:00:00"):
            rs = self.client.post(reverse("auth_password_reset"), rq_body, format="json")
        self.assertEqual(200, rs.status_code)

        with freeze_time("2020-01-01 00:59:00"):
            rs = self.client.post(reverse("auth_password_reset"), rq_body, format="json")

        self.assertEqual(429, rs.status_code)
        self.assertEqual('password_reset_already_in_progress', rs.json()['error'])
        mail.assert_called_once()

    @mock.patch('django.contrib.auth.tokens.PasswordResetTokenGenerator.make_token', side_effect=lambda x: "abc123")
    @mock.patch('utils.mail.send_password_reset_email')
    def test_password_reset_can_be_reseted_again_after_one_hour(self, mail, token_generator):
        rq_body = {"email": self.user.email}

        with freeze_time("2020-01-01 00:00:00"):
            rs = self.client.post(reverse("auth_password_reset"), rq_body, format="json")
        self.assertEqual(200, rs.status_code)

        with freeze_time("2020-01-01 01:00:01"):
            rs = self.client.post(reverse("auth_password_reset"), rq_body, format="json")
        self.assertEqual(200, rs.status_code)

        mail.assert_called_with(self.user, "abc123")
        self.assertEqual(2, token_generator.call_count)
