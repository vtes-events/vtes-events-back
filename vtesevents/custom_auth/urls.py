from django.urls import path
from rest_framework_simplejwt.views import TokenRefreshView

from custom_auth.views import CustomObtainTokenPairView, ResetPasswordView, ResetPasswordConfirmView, ChangePasswordView

urlpatterns = [
    path('auth/login', CustomObtainTokenPairView.as_view(), name='login'),
    path('auth/login/refresh', TokenRefreshView.as_view(), name='login_refresh'),
    path('auth/password/change', ChangePasswordView.as_view(), name='auth_password_change'),
    path('auth/password/reset', ResetPasswordView.as_view(), name='auth_password_reset'),
    path('auth/password/reset/confirm', ResetPasswordConfirmView.as_view(), name='auth_password_reset_confirm'),
]
