from django.contrib.auth.tokens import PasswordResetTokenGenerator
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.generics import GenericAPIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework_simplejwt.views import TokenObtainPairView

from custom_auth.serializers import CustomTokenObtainPairSerializer, ResetPasswordSerializer, \
    ResetPasswordConfirmSerializer, ChangePasswordConfirmSerializer
from users.models import User
from utils import mail
from utils.errors import VTESEventsException
from django.utils import timezone


class CustomObtainTokenPairView(TokenObtainPairView):
    permission_classes = (AllowAny,)
    serializer_class = CustomTokenObtainPairSerializer


class ResetPasswordView(GenericAPIView):
    queryset = User.objects.all()
    serializer_class = ResetPasswordSerializer

    @swagger_auto_schema(responses={200: ""})
    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            email = serializer.validated_data["email"]
            user = User.objects.filter(email=email).first()
            if user:
                if not user.last_password_reset or (timezone.now() - user.last_password_reset).total_seconds() > 3600:
                    self.send_reset_password(user)
                else:
                    raise VTESEventsException(status.HTTP_429_TOO_MANY_REQUESTS, 'password_reset_already_in_progress')
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @staticmethod
    def send_reset_password(user):
        token_generator = PasswordResetTokenGenerator()
        token = token_generator.make_token(user)
        mail.send_password_reset_email(user, token)
        user.last_password_reset = timezone.now()
        user.save()


class ResetPasswordConfirmView(GenericAPIView):
    queryset = User.objects.all()
    serializer_class = ResetPasswordConfirmSerializer

    @swagger_auto_schema(responses={200: ""})
    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            user = User.objects.filter(email=serializer.validated_data["email"]).first()
            user.set_password(serializer.validated_data["new_password"])
            user.save()

            return Response(status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)


class ChangePasswordView(GenericAPIView):
    queryset = User.objects.all()
    serializer_class = ChangePasswordConfirmSerializer
    permission_classes = [IsAuthenticated]

    @swagger_auto_schema(responses={200: ""})
    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            user = self.request.user
            user.set_password(serializer.validated_data["new_password"])
            user.save()

            return Response(status=status.HTTP_200_OK)
        else:
            raise VTESEventsException(status.HTTP_400_BAD_REQUEST, "invalid_password")
