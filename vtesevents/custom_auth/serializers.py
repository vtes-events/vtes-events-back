from django.contrib.auth.tokens import PasswordResetTokenGenerator
from rest_framework import serializers, status
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

from users.models import User
from utils import captcha
from utils.errors import VTESEventsException


class CustomTokenObtainPairSerializer(TokenObtainPairSerializer):
    captcha_token = serializers.CharField()

    @classmethod
    def get_token(cls, user):
        token = super(CustomTokenObtainPairSerializer, cls).get_token(user)

        token['username'] = user.username
        token['email'] = user.email
        token['firstName'] = user.first_name
        token['lastName'] = user.last_name
        token['veknId'] = user.vekn_id

        return token

    def validate(self, attrs):
        if not captcha.is_valid(attrs['captcha_token']):
            raise VTESEventsException(status.HTTP_400_BAD_REQUEST, "invalid_captcha")

        data = super().validate(attrs)

        if not self.user.is_verified():
            raise VTESEventsException(status.HTTP_401_UNAUTHORIZED, "account_not_validated")

        return data


class ResetPasswordSerializer(serializers.Serializer):
    email = serializers.EmailField()


class ResetPasswordConfirmSerializer(serializers.Serializer):
    email = serializers.EmailField()
    token = serializers.CharField()
    new_password = serializers.CharField()

    def validate(self, attrs):
        data = super().validate(attrs)

        user = User.objects.filter(email=attrs["email"]).first()
        if not user:
            raise serializers.ValidationError()

        token_generator = PasswordResetTokenGenerator()
        if not token_generator.check_token(user, attrs["token"]):
            raise serializers.ValidationError()

        return data


class ChangePasswordConfirmSerializer(serializers.Serializer):
    old_password = serializers.CharField()
    new_password = serializers.CharField()

    def validate(self, attrs):
        user = self.context["request"].user
        if not user.check_password(attrs["old_password"]):
            raise serializers.ValidationError({"old_password": "Incorrect password."})

        return attrs
