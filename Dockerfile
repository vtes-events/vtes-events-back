FROM python:3.8-slim-bullseye
ENV DEBUG_MODE False
RUN apt update -y && apt install -y python3-dev default-libmysqlclient-dev build-essential
COPY requirements.txt .
RUN pip3 install -r requirements.txt
COPY vtesevents/ /usr/local/app/
WORKDIR /usr/local/app/
RUN python3 manage.py collectstatic --noinput
EXPOSE 8000
CMD python3 manage.py migrate && gunicorn -w 4 -b 0.0.0.0:8000 vtesevents.wsgi
